### Реализовано по инструкции от `Friflex` на [habr](https://habr.com/ru/companies/friflex/articles/768610/)

### Project name: Sbp pay

### Major changes
|  Build  | Main branch |    Date    | Flutter |       Sdk        | nullsafety | Comment |
|:-------:|:-----------:|:----------:|:-------:|:----------------:|:----------:|:-------:|
| 1.0.1+1 |    main     | 20.03.2024 | 3.19.3  | '>=3.3.1 <4.0.0' |     +      |  init   |


Подключение плагина

<pre><code>
  sbp_pay:
    git:
      url: https://gitlab.com/aleksandr_ozr/sbp_pay.git
      ref: main

</code></pre>



### Дополнительная настройка Android:

#### 1) установить minSdkVersion не ниже 21

#### 2) в своем приложении

 по этому пути: 

`/android/app/src/main/kotlin/[MY_APP]/MainActivity.kt`

необходимо заменить строки

`с этих:`
<pre><code>
import io.flutter.embedding.android.FlutterActivity

class MainActivity: FlutterActivity()

</code></pre>


`на эти:`
<pre><code>
import io.flutter.embedding.android.FlutterFragmentActivity

class MainActivity: FlutterFragmentActivity()

</code></pre>


#### 3) обновить на стили поддерживающие Matrial3.

Не забывайте, при генерации сплешскрина через `flutter_native_splash` эти стили могут затереться

открываем файл

`/android/app/src/main/res/[FOLDER]/styles.xml`

Находим стили, обычно это: `LaunchTheme` и `NormalTheme`

В папке `values` и при наличии в `values-v31`

<pre><code>эту часть
parent="@android:style/Theme.Light.NoTitleBar"

заменяем на
parent="Theme.Material3.Light.NoActionBar"
</code></pre>


В папке `values-night` и при наличии в `values-night-v31`

<pre><code>эту часть
parent="@android:style/Theme.Black.NoTitleBar"

заменяем на
parent="Theme.Material3.Dark.NoActionBar"
</code></pre>




// import 'package:flutter_test/flutter_test.dart';
// import 'package:sbp_pay/sbp_pay.dart';
// // import 'package:sbp_pay/sbp_pay_platform_interface.dart';
// // import 'package:sbp_pay/sbp_pay_method_channel.dart';
// import 'package:plugin_platform_interface/plugin_platform_interface.dart';
//
// class MockSbpPayPlatform
//     with MockPlatformInterfaceMixin
//     implements SbpPayPlatform {
//
//   @override
//   Future<String?> getPlatformVersion() => Future.value('42');
// }
//
// void main() {
//   final SbpPayPlatform initialPlatform = SbpPayPlatform.instance;
//
//   test('$MethodChannelSbpPay is the default instance', () {
//     expect(initialPlatform, isInstanceOf<MethodChannelSbpPay>());
//   });
//
//   test('getPlatformVersion', () async {
//     SbpPay sbpPayPlugin = SbpPay();
//     MockSbpPayPlatform fakePlatform = MockSbpPayPlatform();
//     SbpPayPlatform.instance = fakePlatform;
//
//     expect(await sbpPayPlugin.getPlatformVersion(), '42');
//   });
// }
